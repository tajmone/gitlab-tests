# GitLab Tests

A playground to test GitLab features.


-----

**Table of Contents**

<!-- MarkdownTOC autolink="true" bracket="round" autoanchor="false" lowercase="only_ascii" uri_encoding="true" levels="1,2,3" -->

- [Projects Documentation](#projects-documentation)
    - [GitLab Markup](#gitlab-markup)
    - [GitLab Flavored Markdown \(GFM\)](#gitlab-flavored-markdown-gfm)
    - [Advanced Features in Docs](#advanced-features-in-docs)
        - [PlantUML Diagrams](#plantuml-diagrams)
- [Reference Links](#reference-links)
    - [GitLab Documentation](#gitlab-documentation)

<!-- /MarkdownTOC -->

-----

# Projects Documentation

I want to test the various markups supported in `README` files and other projects documentation files, and investigate the supported features in GitLab's WebUI HTML previewing (and if and how it differs from GitHub, to understand if a document will show up correctly on both platforms).


## GitLab Markup

- https://gitlab.com/gitlab-org/gitlab-markup

[GitLab Markup] is a fork of [GitHub Markup] is the Ruby library used to render all non Markdown markups. It supports the following file extensions/markup syntaxes (used libraries on the righ):

- `.markdown`, `.mdown`, `.mkdn`, `.md` -- via [redcarpet] gem
- `.textile` -- via RedCloth gem
- `.rdoc` -- via rdoc  gem-v 3.6.1
- `.org` -- via org-ruby gem
- `.creole` -- via creole gem
- `.mediawiki`, `.wiki` -- via wikicloth gem
- `.rst` -- via docutils==0.13.1
- `.asciidoc`, `.adoc`, `.asc` -- via [asciidoctor](http://asciidoctor.org)
- `.pod` -- Pod::Simple::HTML comes with Perl >= 5.10. Lower versions should install Pod::Simple from CPAN.


## GitLab Flavored Markdown (GFM)

- [GitLab Flavored Markdown (GFM)][GL GFM]

> _GitLab uses (as of 11.1) the [CommonMark Ruby Library][commonmarker] for Markdown processing of all new issues, merge requests, comments, and other Markdown content in the GitLab system.  As of 11.3, wiki pages and Markdown files (`.md`) in the repositories are also processed with CommonMark.  Older content in issues/comments are still processed using the [Redcarpet Ruby library][redcarpet]._

_Where there are significant differences, we will try to call them out in this document._

GitLab uses "GitLab Flavored Markdown" (GFM). It extends the [CommonMark specification][commonmark-spec] (which is based on standard Markdown) in a few significant ways to add some useful functionality. It was inspired by [GitHub Flavored Markdown](https://help.github.com/articles/basic-writing-and-formatting-syntax/).

See also:

- https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md

## Advanced Features in Docs

### PlantUML Diagrams

- https://docs.gitlab.com/ee/administration/integration/plantuml.html

> When PlantUML integration is enabled and configured in GitLab we are able to create simple diagrams in AsciiDoc and Markdown documents created in snippets, wikis, and repos.


# Reference Links

## GitLab Documentation

- [Project Wikis][GL Wikis]
- [GitLab Flavored Markdown (GFM)][GL GFM]

<!-----------------------------------------------------------------------------
                               REFERENCE LINKS                                
------------------------------------------------------------------------------>

<!-- GitLab Docs Markup Syntaxes -->

[GitLab Markup]: https://gitlab.com/gitlab-org/gitlab-markup
[GitHub Markup]: https://github.com/github/markup

<!-- other -->

[commonmark-spec]: https://spec.commonmark.org/current/
[commonmarker]: https://github.com/gjtorikian/commonmarker
[redcarpet]: https://github.com/vmg/redcarpet "Redcarpet website"
[redcarpet]: https://github.com/vmg/redcarpet "Redcarpet website"
[rouge]: http://rouge.jneen.net/ "Rouge website"

<!-- GitLab Documentation -->

[GL Wikis]: https://docs.gitlab.com/ee/user/project/wiki/ "GitLab Documentation: Projects » Wiki"
[GL GFM]: https://docs.gitlab.com/ee/user/markdown.html "GitLab Documentation:  GitLab Flavored Markdown (GFM)"


<!-- EOF -->
