# Highlight Docs 2 AsciiDoc

In this folder I'm testing the conversion of [Highlight] documentation files to AsciiDoc, to see how they are previewed on GitLab.


-----

**Table of Contents**

<!-- MarkdownTOC autolink="true" bracket="round" autoanchor="false" lowercase="only_ascii" uri_encoding="true" levels="1,2,3" -->

- [Origin and License Note](#origin-and-license-note)
- [AsciiDoc Versions](#asciidoc-versions)
- [Original Docs](#original-docs)

<!-- /MarkdownTOC -->

-----

# Origin and License Note

The original documentation files were taken from [commit `cca88fa6`][cca88fa6] of André Simon's Highlight repository. These files are governed by GPLv3 license:

- [`COPYING`](./COPYING)

The conversion work of this folder was carried out with the author permission (see [Issue #85]).


# AsciiDoc Versions

These are the documents so far converted to AsciiDoc, and their HTML version:

- [`highlight_README.adoc`][README adoc] ([`.html`][README html])


[README adoc]: ./highlight_README.adoc
[README html]: ./highlight_README.html



# Original Docs

These files are the original documentation files, to which I've added the `highlight_` prefix to their names:

- [`highlight_README.md`](./highlight_README.md)
- [`highlight_README_DE.md`](./highlight_README_DE.md)
- [`highlight_README_LANGLIST.md`](./highlight_README_LANGLIST.md)
- [`highlight_README_PLUGINS.md`](./highlight_README_PLUGINS.md)
- [`highlight_README_REGEX.md`](./highlight_README_REGEX.md)
- [`highlight_README_RELEASE.md`](./highlight_README_RELEASE.md)
- [`highlight_README_TESTCASES.md`](./highlight_README_TESTCASES.md)

Although they have `.md` extensions, these are not markdown documents.

<!-----------------------------------------------------------------------------
                               REFERENCE LINKS                                
------------------------------------------------------------------------------>

[Highlight]: https://gitlab.com/saalen/highlight

[cca88fa6]: https://gitlab.com/saalen/highlight/tree/cca88fa6

[Issue #85]: https://gitlab.com/saalen/highlight/issues/85

<!-- EOF -->
